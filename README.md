# Pinch To Zoom in Firefox for OS X
[![build status](https://ci.gitlab.com/projects/2261/status.png?ref=master)](https://ci.gitlab.com/projects/2261?ref=master)


I wrote this addon to allow me pinch to zoom functionnality that was disabled in OS X Yosemite.

I saw this [article](http://applehelpwriter.com/2014/04/30/enable-trackpad-zoom-in-firefox/) which help me to enable the functionnality.

There is also an extension which is not working on Yosemite.

*The github repo is just a mirror of the gitlab repo located at : https://gitlab.com/Fenkiou/pinch-to-zoom-firefox-osx*
