var { get, set } = require("sdk/preferences/service");
var { when: unload } = require("sdk/system/unload");

var main = require("../index");

exports["test main"] = function(assert) {
  var pinch_in_value = get("browser.gesture.pinch.in");
  assert.ok(pinch_in_value == "cmd_fullZoomReduce",
      "Good value for pinch.in !");

  var pinch_in_shift_value = get("browser.gesture.pinch.in.shift");
  assert.ok(pinch_in_shift_value == "cmd_fullZoomReset",
      "Good value for pinch.in.shift !");

  var pinch_out_value = get("browser.gesture.pinch.out");
  assert.ok(pinch_out_value == "cmd_fullZoomEnlarge",
      "Good value for pinch.out !");

  var pinch_out_shift_value = get("browser.gesture.pinch.out.shift");
  assert.ok(pinch_out_shift_value == "cmd_fullZoomReset",
      "Good value for pinch.out.shift !");

  var pinch_latched_value = get("browser.gesture.pinch.latched");
  assert.ok(pinch_latched_value == false,
      "Good value for pinch.latched !");

  var pinch_threshold = get("browser.gesture.pinch.threshold");
  assert.ok(pinch_threshold == 60,
      "Good value for pinch.threshold !");
};

exports["test main async"] = function(assert, done) {
  var pinch_in_value = get("browser.gesture.pinch.in");
  assert.ok(pinch_in_value == "cmd_fullZoomReduce",
      "Good value for pinch.in !");

  var pinch_in_shift_value = get("browser.gesture.pinch.in.shift");
  assert.ok(pinch_in_shift_value == "cmd_fullZoomReset",
      "Good value for pinch.in.shift !");

  var pinch_out_value = get("browser.gesture.pinch.out");
  assert.ok(pinch_out_value == "cmd_fullZoomEnlarge",
      "Good value for pinch.out !");

  var pinch_out_shift_value = get("browser.gesture.pinch.out.shift");
  assert.ok(pinch_out_shift_value == "cmd_fullZoomReset",
      "Good value for pinch.out.shift !");

  var pinch_latched_value = get("browser.gesture.pinch.latched");
  assert.ok(pinch_latched_value == false,
      "Good value for pinch.latched !");

  var pinch_threshold = get("browser.gesture.pinch.threshold");
  assert.ok(pinch_threshold == 60,
      "Good value for pinch.threshold !");

  done();
};

exports["test unload"] = function(assert) {
  main.onUnload();

  var pinch_in_value = get("browser.gesture.pinch.in");
  assert.ok(pinch_in_value == "",
      "Good value for pinch.in !");

  var pinch_in_shift_value = get("browser.gesture.pinch.in.shift");
  assert.ok(pinch_in_shift_value == "",
      "Good value for pinch.in.shift !");

  var pinch_out_value = get("browser.gesture.pinch.out");
  assert.ok(pinch_out_value == "",
      "Good value for pinch.out !");

  var pinch_out_shift_value = get("browser.gesture.pinch.out.shift");
  assert.ok(pinch_out_shift_value == "",
      "Good value for pinch.out.shift !");

  var pinch_latched_value = get("browser.gesture.pinch.latched");
  assert.ok(pinch_latched_value == true,
      "Good value for pinch.latched !");

  var pinch_threshold = get("browser.gesture.pinch.threshold");
  assert.ok(pinch_threshold == 150,
      "Good value for pinch.threshold !");
}

exports["test unload async"] = function(assert, done) {
  main.onUnload();

  var pinch_in_value = get("browser.gesture.pinch.in");
  assert.ok(pinch_in_value == "",
      "Good value for pinch.in !");

  var pinch_in_shift_value = get("browser.gesture.pinch.in.shift");
  assert.ok(pinch_in_shift_value == "",
      "Good value for pinch.in.shift !");

  var pinch_out_value = get("browser.gesture.pinch.out");
  assert.ok(pinch_out_value == "",
      "Good value for pinch.out !");

  var pinch_out_shift_value = get("browser.gesture.pinch.out.shift");
  assert.ok(pinch_out_shift_value == "",
      "Good value for pinch.out.shift !");

  var pinch_latched_value = get("browser.gesture.pinch.latched");
  assert.ok(pinch_latched_value == true,
      "Good value for pinch.latched !");

  var pinch_threshold = get("browser.gesture.pinch.threshold");
  assert.ok(pinch_threshold == 150,
      "Good value for pinch.threshold !");

  done();
}

exports["test threshold change"] = function(assert) {
  require('sdk/simple-prefs').prefs.threshold = 150;
  main.onPrefChange("threshold");

  var value = require('sdk/simple-prefs').prefs.threshold;
  var global_value = get("browser.gesture.pinch.threshold");

  assert.ok(value == global_value, "Threshold value successfully changed !");
}

exports["test threshold change async"] = function(assert, done) {
  require('sdk/simple-prefs').prefs.threshold = 150;
  main.onPrefChange("threshold");

  var value = require('sdk/simple-prefs').prefs.threshold;
  var global_value = get("browser.gesture.pinch.threshold");

  assert.ok(value == global_value, "Threshold value successfully changed !");

  done();
}

require("sdk/test").run(exports);
