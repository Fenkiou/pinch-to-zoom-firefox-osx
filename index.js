var { get, set } = require("sdk/preferences/service");
var { when: unload } = require("sdk/system/unload");

var old_pinch_in_value = get("browser.gesture.pinch.in");
var old_pinch_in_shift_value = get("browser.gesture.pinch.in.shift");
var old_pinch_out_value = get("browser.gesture.pinch.out");
var old_pinch_out_shift_value = get("browser.gesture.pinch.out.shift");
var old_pinch_latched_value = get("browser.gesture.pinch.latched");
var old_pinch_threshold = get("browser.gesture.pinch.threshold");

set("browser.gesture.pinch.in", "cmd_fullZoomReduce");
set("browser.gesture.pinch.in.shift", "cmd_fullZoomReset");
set("browser.gesture.pinch.out", "cmd_fullZoomEnlarge");
set("browser.gesture.pinch.out.shift", "cmd_fullZoomReset");
set("browser.gesture.pinch.latched", false);
set("browser.gesture.pinch.threshold", 60);

// By AMO policy global preferences must be changed back to their original value
exports.onUnload = function (reason) {
  set("browser.gesture.pinch.in", old_pinch_in_value);
  set("browser.gesture.pinch.in.shift", old_pinch_in_shift_value);
  set("browser.gesture.pinch.out", old_pinch_out_value);
  set("browser.gesture.pinch.out.shift", old_pinch_out_shift_value);
  set("browser.gesture.pinch.latched", old_pinch_latched_value);
  set("browser.gesture.pinch.threshold", old_pinch_threshold);
};

exports.onPrefChange = function (prefName) {
  set("browser.gesture.pinch.threshold",
      require('sdk/simple-prefs').prefs.threshold);
};

require("sdk/simple-prefs").on("threshold", require("./index").onPrefChange);
